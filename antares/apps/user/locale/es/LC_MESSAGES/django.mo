��          �   %   �      @  [   A  D   �  I   �  7   ,  ;   d  5   �  0   �  &     /   .  6   ^  9   �  @   �  ,     3   =  (   q  /   �  4   �  ;   �  (   ;  /   d  1   �  8   �  -   �  4   -  k  b  1   �      	  *    	     K	     ^	     l	     z	     �	     �	     �	     �	     �	     �	     �	     
     
     !
     4
     I
     Q
      Z
  $   {
     �
     �
                                                                    	                
                                          antares.apps.user.api.api_on_behalf_change_client_view.client_successfully_changed {client} antares.apps.user.console.user_console.missing_parameter {parameter} antares.apps.user.console.user_console.the_user_id_is {username} {userid} antares.apps.user.constants.ApplicationScopeType.DIALOG antares.apps.user.constants.ApplicationScopeType.NEW_WINDOW antares.apps.user.constants.ApplicationScopeType.SELF antares.apps.user.login.template.forgot_password antares.apps.user.login.template.login antares.apps.user.models.application.table_name antares.apps.user.models.application.table_name_plural antares.apps.user.models.application_parameter.table_name antares.apps.user.models.application_parameter.table_name_plural antares.apps.user.models.org_unit.table_name antares.apps.user.models.org_unit.table_name_plural antares.apps.user.models.role.table_name antares.apps.user.models.role.table_name_plural antares.apps.user.models.role_application.table_name antares.apps.user.models.role_application.table_name_plural antares.apps.user.models.user.table_name antares.apps.user.models.user.table_name_plural antares.apps.user.models.user_org_unit.table_name antares.apps.user.models.user_org_unit.table_name_plural antares.apps.user.models.user_role.table_name antares.apps.user.models.user_role.table_name_plural Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-08-22 18:23-0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 El cliente {client} ha sido cambiado exitosamente Falta el parámetro {parameter} El ID de Usuario de {username} es {userid} Cuadro de diálogo Ventana nueva Misma ventana Olvidó la contraseña? Entrar Aplicación Aplicaciones Parámetro de Aplicación Parámetros de Aplicación Unidad organizacional Unidades organizacionales Rol Roles Rol de Aplicación Roles de Aplicación Usuario Usuarios Unidad Organizacional de Usuario Unidades Organizacionales de Usuario Rol de Usuario Roles de Usuario 