��          �   %   �      @  [   A  D   �  I   �  7   ,  ;   d  5   �  0   �  &     /   .  6   ^  9   �  @   �  ,     3   =  (   q  /   �  4   �  ;   �  (   ;  /   d  1   �  8   �  -   �  4   -  g  b  $   �  $   �  &   	     ;	  
   B	     M	     R	     c	     j	     v	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     +
  	   E
  
   O
                                                                    	                
                                          antares.apps.user.api.api_on_behalf_change_client_view.client_successfully_changed {client} antares.apps.user.console.user_console.missing_parameter {parameter} antares.apps.user.console.user_console.the_user_id_is {username} {userid} antares.apps.user.constants.ApplicationScopeType.DIALOG antares.apps.user.constants.ApplicationScopeType.NEW_WINDOW antares.apps.user.constants.ApplicationScopeType.SELF antares.apps.user.login.template.forgot_password antares.apps.user.login.template.login antares.apps.user.models.application.table_name antares.apps.user.models.application.table_name_plural antares.apps.user.models.application_parameter.table_name antares.apps.user.models.application_parameter.table_name_plural antares.apps.user.models.org_unit.table_name antares.apps.user.models.org_unit.table_name_plural antares.apps.user.models.role.table_name antares.apps.user.models.role.table_name_plural antares.apps.user.models.role_application.table_name antares.apps.user.models.role_application.table_name_plural antares.apps.user.models.user.table_name antares.apps.user.models.user.table_name_plural antares.apps.user.models.user_org_unit.table_name antares.apps.user.models.user_org_unit.table_name_plural antares.apps.user.models.user_role.table_name antares.apps.user.models.user_role.table_name_plural Project-Id-Version: 1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-08-22 18:23-0300
PO-Revision-Date: 2016-07-19 01:34-0300
Last-Translator: Leonardo Belen <leobelen@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Client {client} successfully changed The parameter {parameter} is missing The user id for {username} is {userid} Dialog New Window Self Forgot Password? Log In Application Applications Application Parameter Application Parameters Organizational Unit Organizational Units Role Roles Application Role Application Roles User Users User Organizational Unit User Organizational Units User Role User Roles 