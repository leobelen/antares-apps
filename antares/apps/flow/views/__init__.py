from .inbox_view import InboxView
from .workspace_view import WorkspaceView
from .latest_activities_view import LatestActivitiesView

__all__ = [
    'WorkspaceView',
    'InboxView',
    'LatestActivitiesView',
]
