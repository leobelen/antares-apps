# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-08 20:37
from __future__ import unicode_literals

from ..constants import *
from django.db import migrations
import enumfields.fields


class Migration(migrations.Migration):

    dependencies = [
        ('flow', '0005_auto_20170608_1519'),
    ]

    operations = [
        migrations.RenameField(
            model_name='activitydefinition',
            old_name='instantation',
            new_name='instantiation', ),
        migrations.AlterField(
            model_name='flowdefinition',
            name='priority',
            field=enumfields.fields.EnumField(
                blank=True, enum=FlowPriorityType, max_length=10, null=True),
        ),
    ]
