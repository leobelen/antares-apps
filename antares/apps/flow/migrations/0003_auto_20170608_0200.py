# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-08 05:00
from __future__ import unicode_literals

from antares.apps.core.constants import *
from ..constants import *
from django.db import migrations, models
import enumfields.fields


class Migration(migrations.Migration):

    dependencies = [
        ('flow', '0002_auto_20170427_0107'),
    ]

    operations = [
        migrations.AddField(
            model_name='activitydefinition',
            name='cost',
            field=models.FloatField(blank=True, null=True), ),
        migrations.AddField(
            model_name='activitydefinition',
            name='duration',
            field=models.FloatField(blank=True, null=True), ),
        migrations.AddField(
            model_name='activitydefinition',
            name='instantation',
            field=enumfields.fields.EnumField(
                blank=True,
                enum=FlowActivityInstantiationType,
                max_length=10,
                null=True), ),
        migrations.AddField(
            model_name='activitydefinition',
            name='waiting_time',
            field=models.FloatField(blank=True, null=True), ),
        migrations.AddField(
            model_name='activitydefinition',
            name='working_time',
            field=models.FloatField(blank=True, null=True), ),
        migrations.AddField(
            model_name='flowdefinition',
            name='duration',
            field=models.FloatField(blank=True, null=True), ),
        migrations.AddField(
            model_name='flowdefinition',
            name='priority',
            field=models.CharField(blank=True, max_length=100, null=True), ),
        migrations.AddField(
            model_name='flowdefinition',
            name='time_unit',
            field=enumfields.fields.EnumField(
                blank=True, enum=TimeUnitType, max_length=10, null=True), ),
        migrations.AddField(
            model_name='flowdefinition',
            name='waiting_time',
            field=models.FloatField(blank=True, null=True), ),
        migrations.AddField(
            model_name='flowdefinition',
            name='working_time',
            field=models.FloatField(blank=True, null=True), ),
    ]
