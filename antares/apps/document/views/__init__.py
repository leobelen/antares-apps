from .document_create_view import DocumentCreateView
from .document_edit_view import DocumentEditView
from .document_view_view import DocumentViewView

__all__ = [
    'DocumentEditView',
    'DocumentCreateView',
    'DocumentViewView',
]
