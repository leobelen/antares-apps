from django.contrib import admin

from .models import ObligationRule

admin.site.register(ObligationRule)
