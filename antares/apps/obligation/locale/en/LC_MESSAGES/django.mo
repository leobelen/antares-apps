��          �      �        R   	  K   \  @   �  @   �  C   *  >   n  5   �  7   �  4     9   P  @   �  M   �  L     /   f  7   �  0   �  4   �  0   4  /   e  .   �  .   �  ,   �  �        �     �  	   �  	   �     �     �     �     	     
	     	     	     /	     M	     j	     r	     �	     �	     �	     �	     �	     �	     �	                          	   
                                                                           antares.apps.obligation.console.obligation_console.console.command_sucessfully_run antares.apps.obligation.console.obligation_console.console.dont_now_command antares.apps.obligation.constants.ObligationStatusType.Cancelled antares.apps.obligation.constants.ObligationStatusType.Compliant antares.apps.obligation.constants.ObligationStatusType.Not Exigible antares.apps.obligation.constants.ObligationStatusType.Pending antares.apps.obligation.constants.ObligationType.File antares.apps.obligation.constants.ObligationType.Inform antares.apps.obligation.constants.ObligationType.Pay antares.apps.obligation.models.obligation_rule.table_name antares.apps.obligation.models.obligation_rule.table_name_plural antares.apps.obligation.templates.emptyMessages.no_complied_obligations_found antares.apps.obligation.templates.emptyMessages.no_pending_obligations_found antares.apps.obligation.templates.panel.actions antares.apps.obligation.templates.panel.compliance_date antares.apps.obligation.templates.panel.complied antares.apps.obligation.templates.panel.concept_type antares.apps.obligation.templates.panel.due_date antares.apps.obligation.templates.panel.pending antares.apps.obligation.templates.panel.period antares.apps.obligation.templates.panel.status antares.apps.obligation.templates.panel.type Project-Id-Version: 1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-08-22 18:23-0300
PO-Revision-Date: 2016-07-29 02:11-0300
Last-Translator: Leonardo Javier Belen <leobelen@gmail.com>
Language-Team: Leonardo Javier Belen <leobelen@gmail.com>
Language: en 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Command successfully run Do not know command Cancelled Compliant Not Exigible Pending File Inform Pay Obligation Rule Obligation Rules No complied obligations found No pending obligations found Actions Compliance Date Complied Concept Type Due Date Pending Period Status Type 