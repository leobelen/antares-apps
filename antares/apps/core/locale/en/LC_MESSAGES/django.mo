��          �   %   �      `  ?   a  J   �  i   �  ,   V  -   �  .   �  -   �  /     3   >  .   r  5   �  <   �  +     2   @  0   s  7   �  ,   �  3   	  +   =  2   i  ,   �  3   �  4   �  ;   2  2   n  9   �  k  �      G	  ,   h	  ?   �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     '
     0
     =
     K
  
   T
     _
     g
     p
     �
     �
     �
     �
     �
           
                                                                                             	                    antares.apps.core.console.nuke_console.console.dont_now_command antares.apps.core.console.nuke_console.console.package_deleted_sucessfully antares.apps.core.console.nuke_console.messages.the_flow_definitions_could_not_be_deleted {error_message} antares.apps.core.constants.TimeUnitType.DAY antares.apps.core.constants.TimeUnitType.HOUR antares.apps.core.constants.TimeUnitType.MONTH antares.apps.core.constants.TimeUnitType.YEAR antares.apps.core.constants.WeightUnitType.GRAM antares.apps.core.constants.WeightUnitType.KILOGRAM antares.apps.core.constants.WeightUnitType.TON antares.apps.core.models.action_definition.table_name antares.apps.core.models.action_definition.table_name_plural antares.apps.core.models.catalog.table_name antares.apps.core.models.catalog.table_name_plural antares.apps.core.models.concept_type.table_name antares.apps.core.models.concept_type.table_name_plural antares.apps.core.models.currency.table_name antares.apps.core.models.currency.table_name_plural antares.apps.core.models.holiday.table_name antares.apps.core.models.holiday.table_name_plural antares.apps.core.models.hrn_code.table_name antares.apps.core.models.hrn_code.table_name_plural antares.apps.core.models.system_parameter.table_name antares.apps.core.models.system_parameter.table_name_plural antares.apps.core.models.user_parameter.table_name antares.apps.core.models.user_parameter.table_name_plural Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-08-22 18:23-0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Don't know the specified command Package deleted successfully from the system Flow definitions could not be deleted (reason: {error_message}) Day Hour Month Year Gram Kilogram Ton Action Definition Action Definition Catalog Catalogs Concept Type Concept Types Currency Currencies Holiday Holidays Human Readable Code Human Readable Codes System Parameter System Parameters User Parameter User Parameters 